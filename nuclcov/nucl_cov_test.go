package nuclcov

import (
	"math"
	"testing"
)

func TestCov(t *testing.T) {
	nuclPairs := []string{"AT", "AT", "GC", "AT", "GT"}
	alphabet := []byte{'A', 'C', 'G', 'T'}
	nc := New(alphabet)
	for _, pair := range nuclPairs {
		a, b := pair[0], pair[1]
		err := nc.Add(a, b)
		if err != nil {
			t.Errorf("Expect no erorr, but got %v", err)
		}
	}

	xyExpected := 0.0
	xbarExpected := 0.0
	ybarExpected := 0.0
	for i := 0; i < len(nuclPairs); i++ {
		a, b := nuclPairs[i][0], nuclPairs[i][1]
		for j := i + 1; j < len(nuclPairs); j++ {
			c, d := nuclPairs[j][0], nuclPairs[j][1]
			if a != c && b != d {
				xyExpected++
			}
			if a != c {
				xbarExpected++
			}
			if b != d {
				ybarExpected++
			}
		}
	}

	nExpected := (len(nuclPairs) * (len(nuclPairs) - 1)) / 2
	xyExpected = xyExpected / float64(nExpected)
	xbarExpected = xbarExpected / float64(nExpected)
	ybarExpected = ybarExpected / float64(nExpected)

	xyResult, xbarResult, ybarResult, nResult := nc.Cov()
	if nExpected != nResult {
		t.Errorf("Expected n = %d, but got n = %d", nExpected, nResult)
	}

	threshold := 1e-6
	if math.Abs(xyExpected-xyResult) > threshold {
		t.Errorf("Expected xy = %f, but got xy = %f", xyExpected, xyResult)
	}

	if math.Abs(xbarExpected-xbarResult) > threshold {
		t.Errorf("Expected xbar = %f, but got xbar = %f", xbarExpected, xbarResult)
	}

	if math.Abs(ybarExpected-ybarResult) > threshold {
		t.Errorf("Expected ybar = %f, but got ybar = %f", ybarExpected, ybarResult)
	}

	err := nc.Add('a', 'G')
	if err == nil {
		t.Error("Expected error, but got nothing")
	}

	if nc.Count() != len(nuclPairs) {
		t.Errorf("Expected number of pairs = %d, but got %d", len(nuclPairs), nc.Count())
	}
}

func TestAppend(t *testing.T) {
	nuclPairs := []string{"AT", "AT", "GC", "AT", "GT"}
	alphabet := []byte{'A', 'C', 'G', 'T'}
	nc := New(alphabet)
	for _, pair := range nuclPairs {
		a, b := pair[0], pair[1]
		err := nc.Add(a, b)
		if err != nil {
			t.Errorf("Expect no erorr, but got %v", err)
		}
	}

	nc2 := New(alphabet)
	nc2.Add(nuclPairs[1][0], nuclPairs[1][1])

	err := nc.Append(nc2)
	if err != nil {
		t.Errorf("Expect no eror, but got %v", err)
	}

	nc3 := New([]byte{'c'})
	err = nc.Append(nc3)
	if err == nil {
		t.Error("Expect error, but got none")
	}
}
