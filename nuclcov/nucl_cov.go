package nuclcov

import (
	"bytes"
	"fmt"
)

// NuclCov contains covariance of nucleotide acid in a DNA sequence.
type NuclCov struct {
	Doublets []int
	Alphabet []byte
}

// New return a NuclCov given the alphabet.
func New(alphabet []byte) *NuclCov {
	sizeOfAlphabet := len(alphabet)
	nc := NuclCov{Alphabet: alphabet}
	nc.Doublets = make([]int, sizeOfAlphabet*sizeOfAlphabet)
	return &nc
}

// Add insert a pair of nucliotide acids.
// It returns error when the nucliotide acid is not in the alphabet.
func (n *NuclCov) Add(a, b byte) error {
	indexA := bytes.IndexByte(n.Alphabet, a)
	indexB := bytes.IndexByte(n.Alphabet, b)
	sizeOfAlphabet := len(n.Alphabet)
	if indexA >= 0 && indexB >= 0 {
		n.Doublets[indexA*sizeOfAlphabet+indexB]++
		return nil
	}

	var err error
	if indexA < 0 && indexB < 0 {
		err = fmt.Errorf("%c and %c are not in Alphabet: %s", a, b, string(n.Alphabet))
	} else if indexA < 0 {
		err = fmt.Errorf("%c is not in Alphabet: %s", a, string(n.Alphabet))
	} else {
		err = fmt.Errorf("%c is not in Alphabet: %s", b, string(n.Alphabet))
	}

	return err
}

// Count returns the total number of pairs.
func (nc *NuclCov) Count() int {
	n := 0
	for _, a := range nc.Doublets {
		n += a
	}
	return n
}

// Cov returns the covariance.
func (c *NuclCov) Cov() (xy, xbar, ybar float64, n int) {
	sizeOfAlphabet := len(c.Alphabet)
	for i := 0; i < len(c.Doublets); i++ {
		if c.Doublets[i] > 0 {
			for j := i + 1; j < len(c.Doublets); j++ {
				if c.Doublets[j] > 0 {
					if i%sizeOfAlphabet != j%sizeOfAlphabet && i/sizeOfAlphabet != j/sizeOfAlphabet {
						xy += float64(c.Doublets[i] * c.Doublets[j])
					}

					if i/sizeOfAlphabet != j/sizeOfAlphabet {
						xbar += float64(c.Doublets[i] * c.Doublets[j])
					}

					if i%sizeOfAlphabet != j%sizeOfAlphabet {
						ybar += float64(c.Doublets[i] * c.Doublets[j])
					}

					n += c.Doublets[i] * c.Doublets[j]
				}
			}
			n += c.Doublets[i] * (c.Doublets[i] - 1) / 2
		}
	}
	xy = xy / float64(n)
	xbar = xbar / float64(n)
	ybar = ybar / float64(n)
	return
}

func (nc *NuclCov) Append(nc2 *NuclCov) error {
	// Check alphabet
	diffAlphabetError := fmt.Errorf("Different alphbet %s, %s", string(nc.Alphabet), string(nc2.Alphabet))
	if len(nc.Alphabet) != len(nc2.Alphabet) {
		return diffAlphabetError
	}
	for i, a := range nc.Alphabet {
		b := nc2.Alphabet[i]
		if a != b {
			return diffAlphabetError
		}
	}

	for i := 0; i < len(nc.Doublets); i++ {
		nc.Doublets[i] += nc2.Doublets[i]
	}

	return nil
}
