package storage

// MapStorage uses go built-in map to store key-value pair.
type MapStorage struct {
	m map[string][]byte
}

// NewMapStorage return a MapStorage.
func NewMapStorage() *MapStorage {
	m := MapStorage{}
	m.m = make(map[string][]byte)
	return &m
}

// Put puts a Unit into the storage.
func (m *MapStorage) Put(key string, value []byte) error {
	m.m[key] = value
	return nil
}

// Get gets a Unit given the key.
// It returns nil, when the Unit is not found.
func (m *MapStorage) Get(key string) []byte {
	u, found := m.m[key]
	if found {
		return u
	} else {
		return nil
	}
}

// Keys returns all keys in the storage.
func (m *MapStorage) Keys() []string {
	keys := []string{}
	for key := range m.m {
		keys = append(keys, key)
	}
	return keys
}

// Close
func (m *MapStorage) Close() {

}
