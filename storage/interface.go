package storage

// A type, typically a map or key-value db, that satifies storage.Interface can be
// used to store Unit.
type Interface interface {
	Put(key string, values []byte) error
	Get(key string) (values []byte)
	Keys() []string
	Close()
}
