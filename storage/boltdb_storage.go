package storage

import (
	"github.com/boltdb/bolt"
)

// BoltStorage use bolt to store key-pair.
type BoltStorage struct {
	db         *bolt.DB
	bucketName string
}

// NewBoltStorage return an created/open bolt db
// given the path.
func NewBoltStorage(path string) *BoltStorage {
	b := BoltStorage{}
	b.bucketName = "storage"

	// Create bolt db.
	db, err := bolt.Open(path, 0600, nil)
	if err != nil {
		panic(err)
	}

	// Create bucket.
	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(b.bucketName))
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		panic(err)
	}

	b.db = db

	return &b
}

// Close close the bolt db.
func (b *BoltStorage) Close() {
	b.db.Close()
}

// Put puts a Unit to the storage.
func (bs *BoltStorage) Put(key string, value []byte) error {
	fn := func(tx *bolt.Tx) error {
		// Pack unit using msgpack.
		k := []byte(key)
		b := tx.Bucket([]byte(bs.bucketName))
		err := b.Put(k, value)
		return err
	}

	err := bs.db.Update(fn)
	return err
}

// Get gets a Unit given the key.
// It retuns nil, when the Unit is not found.
func (bs *BoltStorage) Get(key string) (value []byte) {
	fn := func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bs.bucketName))
		value = b.Get([]byte(key))
		return nil
	}

	bs.db.View(fn)

	return
}

// Keys returns a list of all keys.
func (bs *BoltStorage) Keys() []string {
	keys := []string{}
	fn := func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bs.bucketName))
		if err := b.ForEach(func(k, v []byte) error {
			key := string(k)
			keys = append(keys, key)
			return nil
		}); err != nil {
			return err
		}
		return nil
	}

	if err := bs.db.View(fn); err != nil {
		panic(err)
	}
	return keys
}
